﻿##################################
# 
# Biwac_ExtProduct Extension (Beta)
# 08.11.2012
#
# @author Christian Hübschi <christian.huebschi@biwac.ch>
#
# Mit dieser Extension sollen Erweiterungen am Produkt zusammengefasst werden. 
# In einer ersten Version kann die Extention erweitert werden um ein Attribut in ein anderes zu kopieren.
# 
# Auslöser ist der product_save_after Event.
#
# Die Extension befindet sich noch in einem sehr frühen Stadium!
# Erste Umsetzung war bei backformen.ch bf_min_sale_qty -> min_sale_qty (StockItem)
#
# Entwickelt und getestet auf Magento CE 1.5.1



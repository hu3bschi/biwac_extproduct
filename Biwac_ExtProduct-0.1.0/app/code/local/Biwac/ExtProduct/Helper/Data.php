<?php
class Biwac_ExtProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * ignore condition
     * @var string
     */
    const PRODUCT_TYPE_SIMPLE     = 'simple-product';

    /**
     * notice condition
     * @var string
     */
    const ATTR_VALIDATION_NUMERIC = 'numeric';


    private $product      = null;
    private $source       = "";
    private $dest         = "";
    private $conditions   = null;
    public  $debug        = false;

    public function copyProductAttribute($product, $source, $dest, $conditions=null){

        //$stockitem = $product->getStockItem();

        //echo "<pre>"; print_r($stockitem->getData()); exit;
        if(false === $this->setProduct($product)){
            return false;
        }

        if(false === $this->setSource($product,$source)){
            return false;
        }

        if(false === $this->setDest($product,$dest)){
            return false;
        }

        // false === Wenn eine Ignore condition zutrifft
        if(false === $this->validate($conditions)){
            return false;
        }

        $this->copy($source,$dest);

        return $this->dest;


    }

    protected function copy($source,$dest){

        $value = current($this->source); // [key] => value

        list($destObj,$destKey) = $this->dest; // [0] => Zielobjekt, [1] => Attributkey

        if(is_array($destObj)){
            $destObj[$destKey] = $value;
        }
        if(is_object($destObj)){
            $destObj->setData($destKey, $value);
        }

        $this->dest = array($destObj, $destKey);

    }

    protected function setProduct($product){

        if($product instanceof Mage_Catalog_Model_Product){
            $this->product = $product;
            return true;
        }
        if($this->debug){
            throw new Exception('Das Product ist keine Instanz von Mage_Catalog_Model_Product.');
        }
        return false;
    }

    protected function setSource($product,$source){

        // Wenn Source kein Wert enthält muss auch nichts kopiert werden.

        if($product->hasData($source)){

            $value = $product->getData($source);
            if( !empty($value) AND $value != ""){
                $this->source = array($source => $value);
                return true;
            }
        }
        if($this->debug){
            throw new Exception('Der zu kopierende Wert ist leer.');
        }
        return false;
    }

    protected function setDest($product,$dest){

        if(is_array($dest)){
            list($destObj, $key) = $dest;
        } else {
            $destObj = $product;
            $key = $dest;
        }
        if(is_array($destObj)){

            if(array_key_exists($key, $destObj)){
                $this->dest = array($destObj,$key);
                return true;
            }
        }

        if(is_object($destObj)){

            if( $destObj->hasData($key) ){
                $this->dest = array($destObj,$key);
                return true;
            }
        }

        if($this->debug){
            throw new Exception('Das Zielattribut existiert nicht');
        }
        return false;

    }

    protected function validate($conditions){

        if( empty($conditions)){
            if($this->debug){
                throw new Exception('Kopiervorgang wird ignoriert weil keine Condition erfasst ist.');
            }
            return false;
        }

        if( !is_array($conditions)){
            $this->conditions = array($conditions);
        }

        $this->conditions = $conditions;


        // Kein Hinweis, nur ignorieren
        if(array_search(self::PRODUCT_TYPE_SIMPLE, $this->conditions) !== FALSE){

            $product_type = $this->product->getTypeId();
            if($product_type !== Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
                if($this->debug){
                    throw new Exception('Kopiervorgang wird ignoriert weil der Artikel nicht vom Typ `simple` ist.');
                }
                return false;
            }

        }

        // Hinweis ausgeben und nichts tun
        if(array_search(self::ATTR_VALIDATION_NUMERIC, $this->conditions) !== FALSE){

            $src_val = current($this->source);
            $src_key = key($this->source);
            if( !is_numeric($src_val)){
                throw new Exception("Der zu kopierende Wert von \"$src_key\" ist keine Zahl: <b>\"$src_val</b>\"");
            }
        }


    }

}
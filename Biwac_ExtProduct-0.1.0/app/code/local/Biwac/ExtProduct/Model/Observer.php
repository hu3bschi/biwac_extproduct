<?php
class Biwac_ExtProduct_Model_Observer
{

    /**
     * Wird ausgeloest wenn in der Admin oder im Produkt Model ein Artikel gespeichert (->save) wird.
     * Dies umfasst das erstellen, aktualisieren und dublizieren.
     * @param mixed $observer
     */
    public function catalog_product_save_before($observer) {

//         echo "<pre>"; print_r($product->getData()); exit;
    }

    /**
     * Wird ausgeloest wenn in der Admin oder im Produkt Model ein Artikel gespeichert (->save) wird.
     * Dies umfasst das erstellen, aktualisieren und dublizieren.
     * @param mixed $observer
     */
    public function catalog_product_save_after($observer) {

        $product            = $observer->getProduct();

        /* @var $helper Biwac_ExtProduct_Helper_Data */
        $helper = Mage::helper('Biwac_ExtProduct/Data');

        $helper->debug = true;

        $source = 'bf_min_qty';

        $stockitem     = $product->getStockItem();
        $stockitem_id  = $stockitem->getId();
        $dest          = array($stockitem,'min_sale_qty');

        $condition = array( $helper::ATTR_VALIDATION_NUMERIC, $helper::PRODUCT_TYPE_SIMPLE );

        try {

            $destObj = $helper->copyProductAttribute($product, $source, $dest, $condition);

            if ($destObj !== false){

                list($stockitem,$key) = $destObj;

                /* Warum auch immmer, das Stockitem Objekt muss erneut geladen
                 * und der Wert muss erneut zugewiesen werden
                 */
                $value = $stockitem->getData($key);

                /* @var $collection Mage_CatalogInventory_Model_Mysql4_Stock_Item_Collection */
                $collection = $stockitem->getCollection();
                $item = $collection->getItemById($stockitem_id);
                $item->setMinSaleQty($value);
                $item->setUseConfigMinSaleQty(0);
                $item->save();

            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                            Mage::helper('Biwac_ExtProduct')->__($e->getMessage())
            );

        }

        //echo "<pre>"; print_r($product->getData()); exit;
        // do something here

    }



}
